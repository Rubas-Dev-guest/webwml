#use wml::debian::template title="De Debian ontwikkelaarshoek" BARETITLE="true"
#use wml::debian::translation-check translation="2a91a8cd41f14166dc4ef23023d68bc2825268ff"

<p>Hoewel de informatie op deze pagina publiek is, zal ze in de eerste plaats
van belang zijn voor de ontwikkelaars van Debian.</p>

<ul class="toc">
<li><a href="#basic">Basisgegevens</a></li>
<li><a href="#packaging">Verpakkingswerkzaamheden</a></li>
<li><a href="#workinprogress">Werk in uitvoering</a></li>
<li><a href="#projects">Projecten</a></li>
<li><a href="#miscellaneous">Varia</a></li>
</ul>

<div id="main">
  <div class="cardleft" id="basic">
  <h2>Basisgegevens</h2>
      <div>
      <dl>
        <dt><a href="$(HOME)/intro/organization">De organisatie van Debian</a></dt>

        <dd>
        Debian heeft veel toegangspunten en veel mensen zijn erbij betrokken.
        Deze pagina legt uit wie u moet contacteren over een specifiek aspect
        van Debian en licht toe wie zou kunnen antwoorden.
        </dd>

        <dt>De mensen</dt>
        <dd>
        Debian wordt gezamenlijk gemaakt door veel mensen die over de hele
        wereld verspreid leven. Aan het <em>verpakkingswerk</em> dragen
        gewoonlijk zowel <a href="https://wiki.debian.org/DebianDeveloper">ontwikkelaars van Debian (Debian Developers - DD)</a>
        (welke volwaardige leden zijn van het Debian-project) als <a
        href="https://wiki.debian.org/DebianMaintainer">onderhouders van
        Debian (Debian Maintainers - DM)</a> bij. Hier vindt u de
        <a href="https://nm.debian.org/public/people/dd_all">lijst met de ontwikkelaars van Debian</a> en de
        <a href="https://nm.debian.org/public/people/dm_all">lijst met
        de onderhouders van Debian</a>, samen met de door hen onderhouden
        pakketten.

        <p>
        U kunt ook de <a href="developers.loc">wereldkaart met de
        ontwikkelaars van Debian</a> bekijken en een aantal
        <a href="https://gallery.debconf.org/">fotoalbums</a>
        over verschillende Debian-evenementen.
        </p>
        </dd>

        <dt><a href="join/">Aansluiten bij Debian</a></dt>

        <dd>
        Het Debian Project bestaat uit vrijwilligers en we zijn doorgaans
        op zoek naar nieuwe ontwikkelaars met een zekere technische kennis,
        belangstelling in vrije software en enige beschikbare tijd. Ook
        u kunt Debian helpen. Ga gewoon naar de pagina waarnaar hierboven
        verwezen wordt.
        </dd>

        <dt><a href="https://db.debian.org/">Ontwikkelaarsgegevensbank</a></dt>
        <dd>
        De gegevensbank bevat basisgegevens die voor iedereen toegankelijk zijn
        en meer persoonlijke gegevens die enkel zichtbaar zijn voor andere
        ontwikkelaars. Gebruik de <a href="https://db.debian.org/">SSL-versie
        </a> om deze te benaderen indien u gaat inloggen.

        <p>Via de gegevensbank kunt u
        <a href="https://db.debian.org/machines.cgi">de lijst van projectmachines zien</a>,
        <a href="extract_key">de GPG-sleutel van elke ontwikkelaar verkrijgen</a>,
        <a href="https://db.debian.org/password.html">uw wachtwoord wijzigen</a>
        en <a href="https://db.debian.org/forward.html">te weten komen hoe u
        mail forwarding (post doorsturen) moet instellen</a> voor uw Debian-account.</p>

        <p>Indien u een van de Debian-machines gaat gebruiken, moet u
        zeker de <a href="dmup">Richtlijnen voor het gebruik van Debian-machines</a> lezen.</p>
        </dd>

        <dt><a href="constitution">De Constitutie</a></dt>
        <dd>
        Dit is het allerbelangrijkste document van de organisatie. Het
        beschrijft de organisatiestructuur voor het nemen van formele
        beslissingen in het project.
        </dd>

        <dt><a href="$(HOME)/vote/">Informatie over verkiezingen</a></dt>
        <dd>
        Alles wat u ooit wilde weten over hoe wij onze leiders kiezen,
        hoe we een keuze maken over onze logo's en in het algemeen,
        hoe we onze stem uitbrengen.
        </dd>
     </dl>

# this stuff is really not devel-only / deze info is niet echt louter voor ontwikkelaars
     <dl>
        <dt><a href="$(HOME)/releases/">Releases</a></dt>

        <dd>
        Dit is de lijst van oude en huidige releases. Over sommige daarvan
        bestaat gedetailleerde informatie op afzonderlijke webpagina's.

        <p>U kunt ook rechtstreeks naar de huidige
        <a href="$(HOME)/releases/stable/">stabiele release</a> gaan en naar de webpagina's van de
        <a href="$(HOME)/releases/testing/">testing-distributie</a>.</p>
        </dd>

        <dt><a href="$(HOME)/ports/">Verschillende architecturen</a></dt>

        <dd>
        Debian kan op verschillende soorten computers gebruikt worden
        (Intel-compatibele was enkel het <em>eerste</em> soort) en de
        onderhouders van onze &lsquo;ports&rsquo; (met <q>port</q> wordt
        het <q>brengen</q> van Debian naar een bepaalde architectuur bedoeld)
        houden ook nuttige webpagina's bij. Ga er eens kijken en misschien
        wilt u zelf ook nog een extra stuk metaal met een vreemde naam
        aanschaffen.
	</dd>
      </dl>
      </div>

  </div>

  <div class="cardright" id="packaging">
     <h2>Verpakkingswerkzaamheden</h2>
     <div>

      <dl>
        <dt><a href="$(DOC)/debian-policy/">Debian beleidshandboek</a></dt>
        <dd>
        Dit handboek beschrijft welke beleidsrichtlijnen gelden voor de
        Debian-distributie. Dit omvat de structuur en de inhoud van het
        Debian-archief, verschillende elementen betreffende het design van
        het besturingssysteem, evenals de technische vereisten waaraan elk
        pakket moet beantwoorden om te kunnen opgenomen worden in de
        distributie.

        <p>In het kort gezegd betekent dit dat u het <strong>moet</strong> lezen.</p>
        </dd>
      </dl>

      <p>Er bestaan verschillende documenten die verband houden met de
      beleidsrichtlijnen, waarin u misschien geïnteresseerd bent, zoals:</p>
      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />De FHS is een standaard in de vorm van een lijst van
            mappen (en bestanden) welke definieert waar de dingen geplaatst
            moeten worden. De beleidsrichtlijnen 3.x vereisen compatibiliteit
            hiermee.
            </li>
        <li>Lijst van <a href="$(DOC)/packaging-manuals/build-essential">build-essential-pakketten</a>
        <br />De build-essential-pakketten zijn pakketten waarvan verwacht
            wordt dat u ze geïnstalleerd heeft voor u probeert een pakket
            te bouwen. Het is een collectie pakketten die u niet moet
            toevoegen aan de regel <code>Build-Depends</code> van uw pakket.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">Menu-systeem</a>
        <br />Programma's met een interface waaraan geen bijzondere
            commandoregelargumenten opgegeven moeten worden voor een
            normale functionering, moeten in het menu opgenomen worden.
            Raadpleeg ook de <a href="$(DOC)/packaging-manuals/menu.html/">documentatie over het menusysteem</a>.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Emacs-beleidsrichtlijnen</a>
        <br />De pakketten die verband houden met Emacs moeten beantwoorden
            aan de in een document vermelde eigen secundaire richtlijnen.</li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Java-beleidsrichtlijnen</a>
        <br />Het voorgestelde equivalent van het bovenstaande voor met Java
            verband houdende pakketten.</li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Perl-beleidsrichtlijnen</a>
        <br />Secundaire richtlijnen voor alles wat te maken heeft met het verpakken van Perl.</li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Python-beleidsrichtlijnen</a>
        <br />Voorgestelde secundaire richtlijnen voor alles wat te maken heeft met het verpakken van Python.</li>
#	<li><a href="https://pkg-mono.alioth.debian.org/cli-policy/">Debian CLI-beleidsrichtlijnen</a>
#	<br />Basisrichtlijnen in verband met het verpakken van Mono, andere CLR's en
#        CLI-gebaseerde toepassingen en bibliotheken</li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Debconf-specificatie</a>
        <br />De specificatie voor "debconf", het subsysteem voor configuratiebeheer.</li>

#        <li><a href="https://dict-common.alioth.debian.org/">Beleidsrichtlijnen voor spellingshulpmiddelen en -woordenboeken</a>
#        <br />Secundaire beleidsrichtlijnen voor woordenboeken en woordenlijsten voor <kbd>ispell</kbd> / <kbd>myspell</kbd>.</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft/html/">Beleidshandboek voor webapps</a> (ontwerp)
#	<br />Secundaire beleidsrichtlijnen voor webgebaseerde toepassingen.</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft-php/html/">PHP-beleidsrichtlijnen</a> (ontwerp)
#	<br />Standaarden voor het verpakken van PHP.</li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Beleidsrichtlijnen voor gegevensbanktoepassingen</a> (ontwerp)
	<br />Een collectie richtlijnen en goede praktijken voor pakketten met gegevensbanktoepassingen.</li>
	<li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Tcl/Tk-beleidsrichtlijnen</a> (ontwerp)
	<br />Secundaire beleidsrichtlijnen voor alles wat te maken heeft met het verpakken van Tcl/Tk.</li>
	<li><a
	href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Debian
	beleidsrichtlijnen voor Ada</a>
	<br />Secundaire beleidsrichtlijnen voor alles wat te maken heeft met het verpakken van Ada.</li>
      </ul>

      <p>Ga ook eens kijken naar de <a href="https://bugs.debian.org/debian-policy">
      voorgestelde bijwerkingen van beleidsrichtlijnen</a>.</p>

      <p>Merk op dat het grootste gedeelte van de oude handleiding voor het creëren van pakketten
      geïntegreerd werd in de recentste versies van het Beleidshandboek.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">
        Referentiehandboek voor ontwikkelaars</a></dt>

        <dd>
        Dit document heeft tot doel een overzicht te bieden van de aanbevolen
        werkwijzen en beschikbare hulpbronnen voor ontwikkelaars van Debian.
        Ook dit is verplicht leesvoer.
        </dd>

        <dt><a href="$(DOC)/manuals/maint-guide/">Gids voor nieuwe ontwikkelaars</a></dt>

        <dd>
        Dit document beschrijft in gewone taal hoe het bouwen van een pakket
        voor Debian verloopt en het is goed gestoffeerd met werkzame
        voorbeelden. Indien u een ontwikkelaar (verpakker) in spe bent, zult
        u hier veel aan hebben.
        </dd>
      </dl>
      </div>

  </div>

  <div class="card" id="workinprogress">
      <h2>Werk&nbsp;in&nbsp;uitvoering</h2>
      <div>

	<dl>
        <dt><a href="testing">De testing-distributie</a></dt>
        <dd>
        De distributie &lsquo;testing&rsquo; is de plaats waar u uw pakketten
        moet krijgen, opdat ze in overweging genomen zouden worden voor
        release wanneer Debian de volgende keer een release uitbrengt.
        </dd>

        <dt><a href="https://bugs.debian.org/release-critical/">Releasekritieke bugs</a></dt>

        <dd>
        Dit is een lijst met bugs die ervoor kunnen zorgen dat een pakket
        verwijderd wordt uit de distributie "testing" of die er in sommige
        gevallen zelfs kunnen toe leiden dat de uitgave van de distributie
        uitgesteld wordt. Bugrapporten met een ernstigheid hoger dan of
        gelijk aan &lsquo;serious&rsquo; (ernstig) komen in aanmerking
        voor de lijst -- zorg voor een zo snel mogelijke reparatie van
        dergelijke bugs in uw pakketten.
        </dd>

        <dt><a href="$(HOME)/Bugs/">Het Bugvolgsysteem</a></dt>
        <dd>
        Het bugvolgsysteem van Debian (Debian Bug Tracking System - BTS) is
        bedoeld voor het rapporteren, bespreken en repareren van bugs. Hier
        kunnen problemen over ongeveer elk onderdeel van Debian gerapporteerd
        worden. Het BTS is zowel voor gebruikers als voor ontwikkelaars
        nuttig.
        </dd>

        <dt>Pakketoverzichten vanuit het perspectief van een ontwikkelaar</dt>
        <dd>
	De webpagina's voor <a href="https://qa.debian.org/developer.php">pakketinformatie</a>
        en <a href="https://tracker.debian.org/">pakketopvolging</a>
        bevatten voor onderhouders een schat aan waardevolle informatie.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">Het pakketvolgsysteem van Debian</a></dt>
        <dd>
        Het pakketvolgsysteem laat ontwikkelaars die op de hoogte willen
        blijven van de ontwikkeling van andere pakketten, toe, om zich in
        te schrijven (via e-mail) op een dienst die hen over de pakketten
        waarop ze zich ingeschreven hebben, kopieën zendt van e-mails van het
        BTS en meldingen van uploads en installaties.
        </dd>

        <dt><a href="wnpp/">Pakketten die hulp kunnen gebruiken</a></dt>
        <dd>
        Toekomstige pakketten en pakketten waaraan gewerkt zou moeten
        worden (Work-Needing and Prospective Packages - WNPP), is een lijst
        van Debian-pakketten die een nieuwe onderhouder nodig hebben en ook
        van pakketten die nog aan Debian toegevoegd moeten worden. Raadpleeg
        deze indien u een pakket wilt creëren, adopteren of als verweesd wilt
        opgeven.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">\
            Het systeem Incoming</a></dt>
        <dd>
        Nieuwe pakketten worden geüpload naar het systeem
        "Incoming" (binnenkomend) op de interne archiefservers.
        Aanvaarde pakketten zijn bijna onmiddellijk
        <a href="https://incoming.debian.org/">beschikbaar via HTTP</a>
        en worden viermaal per dag verspreid naar de
        <a href="$(HOME)/mirror/">spiegelservers</a>.
        <br />
        <strong>Opmerking</strong>: Gezien de aard van "Incoming" raden we
        het spiegelen ervan niet aan.
        </dd>

        <dt><a href="https://lintian.debian.org/">Rapporten van Lintian</a></dt>

        <dd>
        <a href="https://packages.debian.org/unstable/devel/lintian">
        Lintian</a> is een programma dat nagaat of een pakket conform is aan
        de beleidsrichtlijnen. U moet het vóór iedere upload gebruiken.
        Op de hiervoor vermelde pagina vindt u rapporten over elk pakket
        uit de distributie.
        </dd>

        <dt><a href="https://wiki.debian.org/HelpDebian">Debian Hulp</a></dt>
        <dd>
	De Debian wikipagina groepeert raadgevingen voor ontwikkelaars en andere
    personen die een bijdrage leveren.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">\
            De distributie Experimental</a></dt>
        <dd>
        De distributie <em>experimental</em> wordt gebruikt als een tijdelijke
        opslagplaats voor zeer experimentele software. Gebruik de
        <a href="https://packages.debian.org/experimental/">pakketten uit
        <em>experimental</em></a> enkel indien u reeds weet hoe u de pakketten
        uit <em>unstable</em> moet gebruiken.
        </dd>
      </dl>
      </div>

  </div>
  <div class="card" id="projects">
     <h2>Projecten</h2>
     <div>

      <p>Debian is een uitgebreide groep en in die hoedanigheid bestaat
      deze uit verschillende interne groepen en projecten. Deze welke
      een webpagina hebben worden hieronder in chronologische volgorde
      vermeld:</p>
      <ul>
          <li><a href="website/">De Debian webpagina's</a></li>
          <li><a href="https://ftp-master.debian.org/">Het Debian archief</a></li>
          <li><a href="$(DOC)/ddp">Het Debian documentatieproject (DDP)</a></li>
          <li>De <a href="https://qa.debian.org/">kwaliteitsverzekeringsgroep</a> (Quality Assurance)</li>
          <li><a href="$(HOME)/CD/">Debian cd-images</a></li>
          <li>De <a href="https://wiki.debian.org/Keysigning">coördinatiepagina voor het ondertekenen van sleutels</a></li>
          <li><a href="https://wiki.debian.org/DebianIPv6">Het Debian IPv6-project</a></li>
          <li><a href="buildd/">Het Auto-buildernetwerk</a> en zijn
	  <a href="https://buildd.debian.org/">bouwlogboeken</a>.</li>
          <li><a href="$(HOME)/international/l10n/ddtp">Het Debian Description Translation Project - DDTP</a> (project voor de vertaling van pakketbeschrijvingen)</li>
	  <li><a href="debian-installer/">Het installatiesysteem van Debian</a></li>
	  <li><a href="debian-live/">Debian Live</a></li>
	  <li><a href="$(HOME)/women/">Debian-vrouwen</a></li>
	  <li><a href="$(HOME)/blends/">Debian Pure Blends</a> (Specifieke uitgaven van Debian)</li>

	</ul>
	</div>

  </div>


  <div class="card" id="miscellaneous">
      <h2>Varia</h2>
      <div>


      <p>Diverse links:</p>
      <ul>
        <li><a href="https://debconf-video-team.pages.debian.net/videoplayer/">Opnames</a> van onze conferentietoespraken.</li>
        <li><a href="passwordlessssh">Hoe ssh instellen, zodat u niet
            om een wachtwoord gevraagd wordt</a>.</li>
        <li>Hoe u <a href="$(HOME)/MailingLists/HOWTO_start_list">een nieuwe mailinglijst kunt aanvragen</a>.</li>
        <li>Informatie over <a href="$(HOME)/mirror/">een spiegelserver voor Debian opzetten</a>.</li>
        <li>De <a href="https://qa.debian.org/data/bts/graphs/all.png">grafiek van
            alle bugs</a>.</li>
	<li><a href="https://ftp-master.debian.org/new.html">Nieuwe
            pakketten die wachten op opname in Debian</a> (NEW-wachtrij).</li>
        <li><a href="https://packages.debian.org/unstable/main/newpkg">Nieuwe Debian-pakketten
            van de voorbije zeven dagen</a>.</li>
        <li><a href="https://ftp-master.debian.org/removals.txt">Pakketten die
	    verwijderd werden uit Debian</a>.</li>
        </ul>
      </div>

  </div>
</div>
