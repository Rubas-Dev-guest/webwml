#use wml::debian::template title="Het project debian-volatile"
#use wml::debian::release_info
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

<h2>debian-volatile voor eindgebruikers</h2>

<p class="important"><strong>
Het project debian-volatile
is stopgezet bij de uitgave van Debian <q>Squeeze</q>. Zie
<a href="https://lists.debian.org/debian-volatile-announce/2012/msg00000.html">\
deze aankondiging</a> voor details.
</strong></p>

<h3>Wat is debian-volatile?</h3>
<p>Sommige pakketten richten zich op snel evoluerende zaken, zoals het
filteren van spam en het scannen naar virussen, en zelfs als deze gebruik
maken van bijgewerkte data-patronen,
werken ze niet echt naar behoren gedurende de hele
levensduur van een stabiele release. Het project volatile heeft als
hoofddoel systeembeheerders in staat te stellen hun systemen op een
fatsoenlijke en consistente manier bij te werken, zonder de nadelen die
verbonden zijn met het beroep doel op de distibutie <q>unstable</q> en zelfs
zonder nadelen voor de betrokken pakketten. Debian-volatile bevat dus
enkel wijzigingen aan stabiele programma's die noodzakelijk zijn om ze
functioneel te houden.</p>

<h3>Wat is debian-volatile/sloppy?</h3>

<p>Bij pakketten in de afdeling <em>volatile</em> van het archief debian-volatile,
trachten we ervoor te zorgen dat recentere versies geen functionele wijzigingen
met zich meebrengen en dat de systeembeheerder er niet toe verplicht wordt om
het configuratiebestand aan te passen.</p>


<h3>Archief</h3>
<p>Het officiële archief van debian-volatile werd gearchiveerd op
<a href="http://archive.debian.org/debian-volatile/">archive.debian.org</a>.
</p>

<p>De integratie in het hoofdarchief gebeurde bij de uitgave van <q>Squeeze</q>.
Raadpleeg de <a
href="$(HOME)/News/2011/20110215">\
aankondiging</a> voor informatie over hoe u kunt overschakelen op squeeze-updates.
</p>
