# translation of templates.po to தமிழ்
# ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: templates\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2007-11-03 15:28+0530\n"
"Last-Translator: ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>\n"
"Language-Team: தமிழ் <ubuntu-l10n-tam@lists.ubuntu.com>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Tamil\n"
"X-Poedit-Country: INDIA\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Generator: KBabel 1.11.4\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "தேதி"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "காலவரை"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr ""

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "பரிந்துரைகள்"

#: ../../english/template/debian/votebar.wml:25
#, fuzzy
msgid "Withdrawals"
msgstr "திரும்பப் பெறப் பட்டது"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "விவாதம்"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "கட்டமைப்புகள்"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "பரிந்துரைத்தவர்"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "பரிந்துரை அ வினைப் பரிந்துரைத்தவர்"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "பரிந்துரை இ வினைப் பரிந்துரைத்தவர்"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "பரிந்துரை உ வினைப் பரிந்துரைத்தவர்"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "பரிந்துரை எ வினைப் பரிந்துரைத்தவர்"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "பரிந்துரை ஐ வினைப் பரிந்துரைத்தவர்"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "பரிந்துரை ஒ வினைப் பரிந்துரைத்தவர்"

#: ../../english/template/debian/votebar.wml:55
#, fuzzy
msgid "Proposal G Proposer"
msgstr "பரிந்துரை அ வினைப் பரிந்துரைத்தவர்"

#: ../../english/template/debian/votebar.wml:58
#, fuzzy
msgid "Proposal H Proposer"
msgstr "பரிந்துரை அ வினைப் பரிந்துரைத்தவர்"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "நொடிகள்"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "பரிந்துரை அ நொடிகள்"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "பரிந்துரை இ நொடிகள்"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "பரிந்துரை உ நொடிகள்"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "பரிந்துரை எ நொடிகள்"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "பரிந்துரை ஐ நொடிகள்"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "பரிந்துரை ஒ நொடிகள்"

#: ../../english/template/debian/votebar.wml:82
#, fuzzy
msgid "Proposal G Seconds"
msgstr "பரிந்துரை அ நொடிகள்"

#: ../../english/template/debian/votebar.wml:85
#, fuzzy
msgid "Proposal H Seconds"
msgstr "பரிந்துரை அ நொடிகள்"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "எதிர்தரப்பு"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "உரை"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "பரிந்துரை அ"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "பரிந்துரை இ"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "பரிந்துரை உ"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "பரிந்துரை எ"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "பரிந்துரை ஐ"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "பரிந்துரை ஒ"

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "பரிந்துரை அ"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "பரிந்துரை அ"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "தேர்வுகள்"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "திருத்தத்தைப் பரிந்துரைத்தவர்"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "திருத்த நொடிகள்"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "திருத்த உரைகள்"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "திருத்தத்தைப் பரிந்துரைத்தவர் அ"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "திருத்த நொடிகள் அ"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "திருத்த உரை அ"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "திருத்தத்தைப் பரிந்துரைத்தவர் இ"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "திருத்த நொடிகள் இ"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "திருத்த நொடிகள் இ"

#: ../../english/template/debian/votebar.wml:148
#, fuzzy
msgid "Amendment Proposer C"
msgstr "திருத்தத்தைப் பரிந்துரைத்தவர் அ"

#: ../../english/template/debian/votebar.wml:151
#, fuzzy
msgid "Amendment Seconds C"
msgstr "திருத்த நொடிகள் அ"

#: ../../english/template/debian/votebar.wml:154
#, fuzzy
msgid "Amendment Text C"
msgstr "திருத்த உரை அ"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "திருத்தங்கள்"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "நிகழ்பவை"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "பெரும்பான்மைத் தேவை"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "தரவும் புள்ளி விவரமும்"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "குறைந்தபட்ச தேவை"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "குறைந்த பட்ச கலந்தாயவு"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "வாக்கெடுப்பு"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "தருக்கம்"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "விளைவு"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "ஆதரவாளர்களுக்காக&nbsp;காத்திருக்கிறொம்&nbsp;"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "விவாதத்தில்"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "வாக்கெடுப்பு&nbsp;நடைபெறுகிறது "

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "தீர்மானிக்கப்பட்டது"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "திரும்பப் பெறப் பட்டது"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "ஏனைய"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "&nbsp;வாக்களிப்பின்&nbsp;முதற் பக்கத்திற்கு"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "எப்படி"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "பரிந்துரையொன்றை&nbsp;சமர்பிக்கவும்&nbsp;"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "பரிந்துரையொன்றை&nbsp;அமல் படுத்துக&nbsp;"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "பரிந்துரையொன்றை&nbsp;பின்பற்றவும்&nbsp;"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "விளைவொன்றை&nbsp;வாசிக்கவும்&nbsp;"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "வாக்களி"
