msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 16:59+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr ""

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />Oftaj demandoj"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Elŝuti per Jigdo"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "Elŝuti per HTTP/FTP"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "Aĉeti KD-jn aŭ DVD-jn"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "Perreta instalado"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />Elŝuti"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />Alispeciaj"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id=\"dc_artwork\" />Artaĵoj"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />Spegulado"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Speguloj de rsync"

#: ../../english/template/debian/cdimage.wml:43
#, fuzzy
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_verify\" />Kontroli"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />Elŝuti per Torrent"

#: ../../english/template/debian/cdimage.wml:49
msgid "<void id=\"dc_relinfo\" />Image Release Info"
msgstr "<void id=\"dc_relinfo\" />Informoj pri livero de bildo"

#: ../../english/template/debian/cdimage.wml:52
msgid "Debian CD team"
msgstr "Debiana teamo pri KD"

#: ../../english/template/debian/cdimage.wml:55
msgid "debian_on_cd"
msgstr ""

#: ../../english/template/debian/cdimage.wml:58
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />oftaj demandoj"

#: ../../english/template/debian/cdimage.wml:61
msgid "jigdo"
msgstr ""

#: ../../english/template/debian/cdimage.wml:64
msgid "http_ftp"
msgstr ""

#: ../../english/template/debian/cdimage.wml:67
msgid "buy"
msgstr "aĉeti"

#: ../../english/template/debian/cdimage.wml:70
msgid "net_install"
msgstr ""

#: ../../english/template/debian/cdimage.wml:73
msgid "<void id=\"misc-bottom\" />misc"
msgstr ""

#: ../../english/template/debian/cdimage.wml:76
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"<a href=\"/MailingLists/disclaimer\">Publika retpoŝta dissendolisto</a> por "
"KD/DVD en la angla lingvo:"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:13
msgid "      Key fingerprint"
msgstr "      Key fingerprint"

#: ../../english/devel/debian-installer/images.data:87
msgid "ISO images"
msgstr ""

#: ../../english/devel/debian-installer/images.data:88
msgid "Jigdo files"
msgstr ""
