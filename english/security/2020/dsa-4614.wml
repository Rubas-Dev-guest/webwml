<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Joe Vennix discovered a stack-based buffer overflow vulnerability in
sudo, a program designed to provide limited super user privileges to
specific users, triggerable when configured with the <q>pwfeedback</q> option
enabled. An unprivileged user can take advantage of this flaw to obtain
full root privileges.</p>

<p>Details can be found in the upstream advisory at
<a href="https://www.sudo.ws/alerts/pwfeedback.html">https://www.sudo.ws/alerts/pwfeedback.html</a> .</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 1.8.19p1-2.1+deb9u2.</p>

<p>For the stable distribution (buster), exploitation of the bug is
prevented due to a change in EOF handling introduced in 1.8.26.</p>

<p>We recommend that you upgrade your sudo packages.</p>

<p>For the detailed security status of sudo please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/sudo">https://security-tracker.debian.org/tracker/sudo</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4614.data"
# $Id: $
