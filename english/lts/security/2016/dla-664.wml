<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Tobias Stoeckmann from the OpenBSD project has discovered a number of
issues in the way various X client libraries handle the responses they
receive from servers. Insufficient validation of data from the X server
could cause out of boundary memory writes in the libXrender library
potentially allowing the user to escalate their privileges.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:0.9.7-1+deb7u3.</p>

<p>We recommend that you upgrade your libxrender packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-664.data"
# $Id: $
