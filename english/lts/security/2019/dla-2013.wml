<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in libvorbis, a decoder library for Vorbis
General Audio Compression Codec.</p>

<p>The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2017-14160">CVE-2017-14160</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2018-10393">CVE-2018-10393</a> improve the bound checking
for very low sample rates.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10392">CVE-2018-10392</a>

<p>was found because the number of channels was not validated
and a remote attacker could cause a denial of service.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.4-2+deb8u2.</p>

<p>We recommend that you upgrade your libvorbis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2013.data"
# $Id: $
