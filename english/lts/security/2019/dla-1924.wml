<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in Python, an interactive high-level
object-oriented language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16056">CVE-2019-16056</a>

    <p>The email module wrongly parses email addresses that contain
    multiple @ characters. An application that uses the email module and
    implements some kind of checks on the From/To headers of a message
    could be tricked into accepting an email address that should be
    denied.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.4.2-1+deb8u7.</p>

<p>We recommend that you upgrade your python3.4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1924.data"
# $Id: $
