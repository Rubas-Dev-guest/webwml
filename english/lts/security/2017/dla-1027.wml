<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jeffrey Altman, Viktor Duchovni and Nico Williams identified a mutual
authentication bypass vulnerability in Heimdal Kerberos. Also known as
Orpheus' Lyre, this vulnerability could be used by an attacker to mount
a service impersonation attack on the client if he's on the network
path between the client and the service.</p>

<p>More details can be found on the vulnerability website
(https://orpheus-lyre.info/).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.6~git20120403+dfsg1-2+deb7u1.</p>

<p>We recommend that you upgrade your heimdal packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1027.data"
# $Id: $
