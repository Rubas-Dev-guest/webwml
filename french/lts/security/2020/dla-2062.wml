#use wml::debian::translation-check translation="cff50e7edd912de4801dcc8a339e0e35e06e2417" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que sa-exim, le filtre de SpamAssassin pour Exim, permet à
des attaquants d’exécuter du code arbitraire si des utilisateurs sont autorisés
à exécuter des règles personnalisées. Un problème similaire a été corrigé dans
dans spamassassin,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-11805">CVE-2018-11805</a>,
qui causait une régression de fonction dans sa-exim. Cette mise à jour restaure
la compatibilité entre spamassassin et sa-exim. Les implications de sécurité de
la fonction de mise en liste grise sont expliquées dans
/usr/share/doc/sa-exim/README.greylisting.gz.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 4.2.1-14+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sa-exim.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2062.data"
# $Id: $
