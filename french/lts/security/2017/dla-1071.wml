#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans qemu-kvm, une solution
complète de virtualisation pour des hôtes Linux sur du matériel x86 avec des
invités x86 basés sur Qemu (Quick Emulator).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6505">CVE-2017-6505</a>

<p>Déni de service à l’aide d’une boucle infinie dans l’émulation USB OHCI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8309">CVE-2017-8309</a>

<p>Déni de service à l’aide de capture audio VNC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10664">CVE-2017-10664</a>

<p>Déni de service dans le serveur qemu-nbd, qemu-io et qemu-img.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11434">CVE-2017-11434</a>

<p>Déni de service à l'aide d'une chaîne d’option DHCP contrefaite.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.1.2+dfsg-6+deb7u23.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu-kvm.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1071.data"
# $Id: $
