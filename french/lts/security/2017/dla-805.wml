#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de déni de service (échecs d’assertion) ont été
découvertes dans BIND, une implémentation de serveur DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9131">CVE-2016-9131</a>

<p>Une réponse de l’amont contrefaite à n’importe quelle requête pourrait causer
un échec d’assertion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9147">CVE-2016-9147</a>

<p>Une réponse de l’amont contrefaite avec des données DNSSEC auto-incompatibles
pourrait causer un échec d’assertion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9444">CVE-2016-9444</a>

<p>Une réponse de l’amont contrefaite pour l'occasion avec un enregistrement DS
pourrait causer un échec d’assertion.</p></li>

</ul>

<p>Ces vulnérabilités affectent principalement les serveurs DNS fournissant un
service récursif. Les requêtes de client aux serveurs seulement d’autorité ne
peuvent déclencher ces échecs d’assertion. Ces vulnérabilités sont présentes
que la validation DNSSEC soit activée ou non dans la configuration du serveur.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:9.8.4.dfsg.P1-6+nmu2+deb7u14.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-805.data"
# $Id: $
