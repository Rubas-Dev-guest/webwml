#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Request Tracker, un
système paramétrable de suivi de problèmes. Le projet « Common Vulnerabilities
and Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6127">CVE-2016-6127</a>

<p>Request Tracker est vulnérable à une attaque par script intersite (XSS)
si un attaquant charge un fichier malveillant avec un certain type de
contenu. Les installations qui utilisent le paramètre de configuration
AlwaysDownloadAttachments ne sont pas affectées par ce défaut. Le correctif
appliqué traite toutes les pièces attachées existantes et à venir.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5361">CVE-2017-5361</a>

<p>Request Tracker est vulnérable à des attaques temporelles par canal
auxiliaire pour les mots de passe utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5943">CVE-2017-5943</a>

<p>Request Tracker est prédisposé à une fuite d'informations de jetons de
vérification de contrefaçon de requête intersite (CSRF), si un utilisateur
est piégé par un attaquant en visitant une URL contrefaite pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5944">CVE-2017-5944</a>

<p>Request Tracker est prédisposé à une vulnérabilité d'exécution de code à
distance dans l'interface de souscription du tableau de bord. Un attaquant
privilégié peut tirer avantage de ce défaut grâce à des noms de recherches
sauvegardées soigneusement contrefaites pour provoquer l'exécution de code
inattendu. Le correctif appliqué traite toutes les recherches sauvegardées
existantes et à venir.</p>

</ul>

<p>En plus de CVE mentionnées ci-dessus, cette mise à jour contourne le
<a href="https://security-tracker.debian.org/tracker/CVE-2015-7686">CVE-2015-7686</a>
dans Email::Address qui pourrait induire un déni de service de Request
Tracker lui-même.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.0.7-5+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets request-tracker4.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-987.data"
# $Id: $
