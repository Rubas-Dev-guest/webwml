#use wml::debian::translation-check translation="3afa4304414524a10aaabbeb62daa83519b47a4e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Diverses vulnérabilités ont été découvertes dans Samba, le serveur et client
SMB/CIFS de fichiers, d’impressions et de connexions pour Unix</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9461">CVE-2017-9461</a>

<p>smbd dans Samba possédait une vulnérabilité de déni de service (boucle
infinie dans fd_open_atomic avec utilisation élevée de CPU et consommation de
mémoire) due à un mauvais traitement de liens symboliques sans cible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1050">CVE-2018-1050</a>

<p>Samba était vulnérable à une attaque par déni de service lorsque le service
spoolss RPC était configuré pour fonctionner comme démon externe. Des
vérifications manquantes des entrées dans quelques paramètres d’entrée pour les
appels spoolss RPC pouvaient faire planter le service d’impression
désynchronisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1057">CVE-2018-1057</a>

<p>Sur le contrôleur de domaine basé sur Active Directory de Samba 4, le serveur
LDAP valide incorrectement les droits de modification des mots de passe pour
LDAP permettant à des utilisateurs authentifiés de modifier les mots de passe
de n'importe quel autre utilisateur, y compris ceux des administrateurs et des
comptes privilégiés de service (par exemple, contrôleurs de domaine).</p>

<p>Merci à l’équipe de sécurité d’Ubuntu pour le rétroportage plutôt invasif de
l’ensemble de modifications de Samba vers Ubuntu 14.04 (que nous pouvons
utiliser comme corriger Samba dans Debian Jessie LTS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3880">CVE-2019-3880</a>

<p>Un défaut a été découvert dans la façon dont Samba implémente un point
d’entrée RPC émulant l’API de service de registres de Windows. Un attaquant sans
droit pourrait avoir utilisé ce défaut pour créer un nouveau fichier de la base
de registe partout où il avait les permissions Unix. Cela pouvait conduire à la
création d’un nouveau fichier dans le partage Samba.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2:4.2.14+dfsg-0+deb8u12.</p>
<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1754.data"
# $Id: $
