#use wml::debian::translation-check translation="24c857ab9e9769817510eaee4ab4860fd27c3a19" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Joe Vennix a découvert une vulnérabilité de contournement
d'authentification dans dbus, un système asynchrone de communication
inter-processus. L'implémentation du mécanisme d'authentification
DBUS_COOKIE_SHA1 était vulnérable à une attaque par lien symbolique. Un
attaquant local pourrait tirer avantage de ce défaut pour contourner
l'authentification et se connecter à un serveur DBus avec des privilèges
plus élevés.</p>

<p>Le système et les démons de session de dbus standard, dans leur
configuration par défaut, ne sont pas affectés par cette vulnérabilité.</p>

<p>La vulnérabilité est corrigé en mettant dbus à niveau vers la nouvelle
version 1.10.28 de l'amont qui comprend des correctifs supplémentaires.</p>

<p>Pour la distribution stable (Stretch), ce problème a été corrigé dans
la version 1.10.28-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dbus.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de dbus, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/dbus">\
https://security-tracker.debian.org/tracker/dbus</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4462.data"
# $Id: $
